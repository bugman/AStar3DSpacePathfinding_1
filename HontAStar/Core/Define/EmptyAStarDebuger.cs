﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.AStar
{
    public class EmptyAStarDebuger : IAStarStepDebuger
    {
        public void OnAddToClosedList(Position point)
        {
        }

        public void OnPathfindingBegin(Position startPos, Position endPos)
        {
        }

        public void OnToNextPoint(Position nextPoint, DebugerNodeTypeEnum nodeType)
        {
        }
    }
}
